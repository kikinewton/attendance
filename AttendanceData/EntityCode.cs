﻿using System;

namespace HealthMS.Services
{
    public class EntityCode
    {
        public string GenerateCode(string FirstName, string LastName)
        {
            int lenFirstName;
            int lenLastName;

            FirstName = FirstName.ToUpper();
            LastName = LastName.ToUpper();

            lenFirstName = FirstName.Length;
            lenLastName = LastName.Length;

            if (lenLastName == 2)
                LastName = LastName + "xx";

            if (lenFirstName == 2)
                FirstName = FirstName + "xx";

            string PrefixFirstname;
            string PrefixLastname;

            PrefixFirstname = FirstName.Substring(0, 3);
            PrefixLastname = LastName.Substring(0, 3);

            string code;

            var hash = PrefixFirstname.GetHashCode();
            code = PrefixFirstname + PrefixLastname + hash.ToString();


            return code;
        }

    }
}
