﻿using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;

namespace AttendanceData.Entities
{
    public class User : IdentityUser
    {
        public User() => Employees = new List<Employee>();

        public virtual ICollection<Employee> Employees { get; private set; }


    }
}
