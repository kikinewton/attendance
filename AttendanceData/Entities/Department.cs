﻿using System.ComponentModel.DataAnnotations;

namespace AttendanceData.Entities
{
    public class Department
    {
        public int DepartmentId { get; set; }
        
        public string Name { get; set; }

    }
}
