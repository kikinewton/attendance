﻿using System;
using System.ComponentModel.DataAnnotations;

namespace AttendanceData.Entities
{
    public class EmployeeTask
    {
        public int ID { get; set; }
        [Display(Name = "Task"), MaxLength(50)]
        public string TaskType { get; set; }

        public string TodoId { get; set; }
        [MaxLength(256), DataType(DataType.MultilineText)]
        public string Description { get; set; }

        public string UserId { get; set; }
        //[Display(Name = "Start Date & Time"), DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime StartDate { get; set; }
        //[Display(Name = "End Date & Time"), DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime EndDate { get; set; }
        [Display(Name = "Status")]
        public bool IsComplete { get; set; }

        public double Duration { get; set; }
        //public TaskTodo TaskTodo { get; set; }
    }
}
