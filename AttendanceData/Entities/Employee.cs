﻿using System;
using System.ComponentModel.DataAnnotations;

namespace AttendanceData.Entities
{
    public class Employee
    {
        public string UserId { get; set; }
        [Display(Name = "Department")]
        public int DepartmentId { get; set; }
        
        public string EmployeeId { get; set; } = Guid.NewGuid().ToString();
        [Required, Display(Name = "First name")]
        public string FirstName { get; set; }
        [Required, Display(Name = "Last name")]
        public string LastName { get; set; }
        public string Contact { get; set; }
        public string Email { get; set; }
        //public EmployeeType EmpType { get; set; }
        public Sex Gender { get; set; }
        [Required, Display(Name ="Marital Status")]
        public Marital MaritalStatus { get; set; }
        public Jobtitles Jobtitle { get; set; }
        public Department Department { get; set; }
        public User User { get; set; }

        
        public enum Jobtitles
        {
            Intern, Assistant, Associate, Manager
        }
        public enum Sex
        {
            Male, Female
        }

        public enum Marital { Married, Single, Divorced, Other }

        public enum EmployeeType
        {
            Permanent, Contract, Service, Intern
        }
    }
}

