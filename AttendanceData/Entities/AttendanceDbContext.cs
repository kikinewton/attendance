﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

namespace AttendanceData.Entities
{
    public class AttendanceDbContext : IdentityDbContext<User>
    {
        //public AttendanceDbContext()
        //{
        //    Departments = new List<Department>();
        //}
        public AttendanceDbContext(DbContextOptions options) : base(options) { }
        public DbSet<EmployeeTask> EmployeeTasks { get; set; }
        public DbSet<Department> Departments { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<TaskTodo> TaskTodos { get; set; }

    }
}
