﻿using System;
using System.Threading.Tasks;
using Attendance.ViewModel;
using AttendanceData.Entities;
using AttendanceServices.Services;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace Attendance.Controllers
{
    public class AccountController : Controller
    {
        private const string Obj = "Manager";
        private UserManager<User> _userManager;
        private SignInManager<User> _signInManager;
        private readonly AttendanceDbContext _context;
        private readonly IDepartmentData _departmentData;
        private readonly RoleManager<IdentityRole> _roleManager;
        
        public AccountController(UserManager<User> userManager,
                                 SignInManager<User> signInManager,
                                 AttendanceDbContext context,
                                 IDepartmentData departmentData,
                                 RoleManager<IdentityRole> roleManager)
        {
            _userManager = userManager ?? throw new ArgumentNullException(nameof(userManager));
            _signInManager = signInManager;
            _context = context;
            _departmentData = departmentData;
            _roleManager = roleManager;
        }

        //public IActionResult Index()
        //{
        //    return View();
        //}

        [HttpPost, ValidateAntiForgeryToken]
        public async Task<IActionResult> Logout()
        {
            await _signInManager.SignOutAsync();
            return RedirectToAction( "Login", "Account");
        }

        [HttpGet]
        public IActionResult Login()
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "EmployeeTask");
            }

            return View();
        }
        [HttpPost, ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(RegisterUserViewModel registerUserViewModel)
        {
            var dummy = await _userManager.FindByNameAsync(registerUserViewModel.Username);
            if (dummy != null)
            {
                return RedirectToAction("Register", "Account");
            }
            
            if (ModelState.IsValid)
            {
                
                User user = new User { UserName = registerUserViewModel.Username };
                user.Employees.Add(new Employee { FirstName = registerUserViewModel.FirstName,
                    LastName = registerUserViewModel.LastName,
                    Contact = registerUserViewModel.PhoneNumber,
                    Email = registerUserViewModel.Email,
                    Gender = (Employee.Sex)registerUserViewModel.Gender,
                    MaritalStatus = (Employee.Marital)registerUserViewModel.MaritalStatus,
                    Jobtitle = (Employee.Jobtitles)registerUserViewModel.Jobtitle,
                    DepartmentId = registerUserViewModel.Department,
                    UserId = user.Id

                });
                var createUser = await _userManager.CreateAsync(user, registerUserViewModel.Password);
                if (createUser.Succeeded )
                {
                    var jtitle = registerUserViewModel.Jobtitle.ToString();

                    if (string.Equals(registerUserViewModel.Jobtitle.ToString(), Obj))
                    {
                        if (!await _roleManager.RoleExistsAsync("Admin"))
                        {
                            var user_role = new IdentityRole("Admin");
                            var result = await _roleManager.CreateAsync(user_role);
                            if (result.Succeeded)
                            {
                                await _userManager.AddToRoleAsync(user, "Admin");
                            }
                        }

                    }

                    await _signInManager.SignInAsync(user, false);
                    return RedirectToAction("Index", "EmployeeTask");
                }
                else
                {
                    foreach (var error in createUser.Errors)
                    {
                        ModelState.AddModelError("", error.Description);
                    }
                }
            }

            return View();
        }

        [HttpGet]
        public IActionResult Register()
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "EmployeeTask");
            }
            var department = _context.Departments;
            ViewBag.Department = department;
            return View();
        }

        [HttpPost, ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                var loginResult = await _signInManager.PasswordSignInAsync(
                                                       model.Username, model.Password,
                                                       model.RememberMe, false);
                if (loginResult.Succeeded)
                {
                    if (Url.IsLocalUrl(model.ReturnUrl))
                    {
                        return Redirect(model.ReturnUrl);
                    }
                    else
                    {
                        return RedirectToAction("Index", "EmployeeTask");
                    }
                }

            }
            ModelState.AddModelError("", "Could not login");
            return View(model);

        }

    }
}