﻿using System;
using System.Collections.Generic;
using Attendance.ViewModel;
using AttendanceData.Entities;
using AttendanceServices.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Attendance.Controllers
{
    [Authorize]
    public class ChartsController : Controller
    {
        public ChartsController(IEmployeeTaskData employeeTaskData, AttendanceDbContext context, IEmployeeData employeeData)
        {
            _emptaskData = employeeTaskData;
            _context = context;
            _employee = employeeData;
        }

        private Random rnd = new Random();
        private IEmployeeTaskData _emptaskData;
        private AttendanceDbContext _context;
        private IEmployeeData _employee;

        public IActionResult Bar()
        {
            var model = _context.Employees;
            var lstModel = new List<SimpleReportViewModel>();

            foreach (var item in model)
            {
                var report = new SimpleReportViewModel
                {
                    DimensionOne = item.LastName + " " + item.FirstName,
                    Quantity = _emptaskData.GetTaskByUserId(item.UserId)
                };
                lstModel.Add(report);
            }

            return View(lstModel);
        }



        public IActionResult Pie()
        {
            var _task = _context.TaskTodos;
            var lstModel = new List<SimpleReportViewModel>();
            foreach(var item in _task)
            {
                lstModel.Add(new SimpleReportViewModel
                {
                    DimensionOne = item.Type,
                    Quantity = _emptaskData.GetDurationByProject(item.TaskTodoID)
                });
            }


            return View(lstModel);
        }
    }
}