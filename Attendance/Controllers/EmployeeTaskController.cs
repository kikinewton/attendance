﻿using Attendance.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using AttendanceServices.Services;
using Attendance.ViewModel;
using AttendanceData.Entities;
using System.Security.Claims;
using System.Linq;
using System;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;
using Attendance.Misc;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace Attendance.Controllers
{
    [Authorize]
    public class EmployeeTaskController : Controller
    {
        private IEmployeeTaskData _data;
        private UserManager<User> _userManager;
        private AttendanceDbContext _context;
        private ITaskTodoData _tasktododata;

        //private IEmployeeData _emp_data;

        public EmployeeTaskController(IEmployeeTaskData employeeTaskData, ITaskTodoData taskTodoData, AttendanceDbContext context, UserManager<User> userManager)
        {
            _data = employeeTaskData;
            _userManager = userManager;
            _context = context;
            //_emp_data = employeeData;
            _tasktododata = taskTodoData;
        }
        // GET: EmployeeTask
        public ActionResult Index()
        {
            //var _task = from t in _context.EmployeeTasks select t;
            //int pageSize = 10;

            var userID = _userManager.GetUserId(HttpContext.User);
            ViewBag.UserID = userID;
            var emp_task = new EmployeeTaskModel();
            emp_task.employeeTasks = _data.GetAll();

            //return View(await PaginatedList<EmployeeTask>.CreateAsync(_task.AsNoTracking(),page ?? 1, pageSize));
            return View(emp_task);
        }

        // GET: EmployeeTask/Details/5
       
        public async Task<ActionResult> Details(int? id)
        {
            
            if (id == null)
            {
                _ = RedirectToAction(nameof(Index));
            }
            var model = await _context.EmployeeTasks.FirstOrDefaultAsync(x => x.ID == id);
            {
                if(model == null)
                {
                    return NotFound();
                }
            }

            return View(model);
        }

        // GET: EmployeeTask/Create
        public ActionResult Create()
        {

            var _taskType = _context.TaskTodos;
            ViewBag.TaskTodo = _taskType;
            return View();
        }

        // POST: EmployeeTask/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(EmployeeTaskViewModel employeeTaskViewModel)
        {
            try
            {
                // TODO: Add insert logic here
                if (ModelState.IsValid)
                {
                    var _emp_task = new EmployeeTask
                    {
                        Description = employeeTaskViewModel.Description,
                        UserId = _userManager.GetUserId(HttpContext.User),
                        TodoId = _tasktododata.GetTaskId(employeeTaskViewModel.TaskType),
                        TaskType = employeeTaskViewModel.TaskType,
                        StartDate = employeeTaskViewModel.StartDate,
                        EndDate = employeeTaskViewModel.EndDate,
                        IsComplete = employeeTaskViewModel.IsComplete, 
                        Duration = Duration(employeeTaskViewModel.StartDate, employeeTaskViewModel.EndDate)
                        
                    };
                    _data.Add(_emp_task);
                    return RedirectToAction(nameof(Index));

                }

                return RedirectToAction(nameof(Index));
            }
            catch(Exception ex)
            {
                var error = ex.Message;
                return View();
            }
        }

        private double Duration(DateTime startTime, DateTime endTime) => (endTime - startTime).TotalMinutes;
        

        // GET: EmployeeTask/Edit/5
        [HttpGet]
        [Authorize]
        public ActionResult Edit(int id)
        {
            var model = _data.GetById(id);
            if (model == null)
            {
                return RedirectToAction("Index");
            }
            else
            {
                return View(model);
            }
            
        }

        // POST: EmployeeTask/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, EmployeeTaskViewModel model)
        {
            
            try
            {
                var emp_task = _data.GetById(id);
                if (ModelState.IsValid)
                {
                    emp_task.IsComplete = model.IsComplete;
                    _data.Commit();
                    return RedirectToAction("Details", new { id = emp_task.ID});
                }

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: EmployeeTask/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: EmployeeTask/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}