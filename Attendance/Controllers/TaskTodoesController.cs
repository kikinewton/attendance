﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using AttendanceData.Entities;
using Microsoft.AspNetCore.Authorization;

namespace Attendance.Controllers
{
    [Authorize]
    public class TaskTodoesController : Controller
    {
        private readonly AttendanceDbContext _context;

        public TaskTodoesController(AttendanceDbContext context)
        {
            _context = context;
        }

        // GET: TaskTodoes
        public async Task<IActionResult> Index()
        {
            return View(await _context.TaskTodos.ToListAsync());
        }

        // GET: TaskTodoes/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var taskTodo = await _context.TaskTodos
                .FirstOrDefaultAsync(m => m.TaskTodoID == id);
            if (taskTodo == null)
            {
                return NotFound();
            }

            return View(taskTodo);
        }

        // GET: TaskTodoes/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: TaskTodoes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("TaskTodoID,Type")] TaskTodo taskTodo)
        {
            if (ModelState.IsValid)
            {
                _context.Add(taskTodo);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(taskTodo);
        }

        // GET: TaskTodoes/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var taskTodo = await _context.TaskTodos.FindAsync(id);
            if (taskTodo == null)
            {
                return NotFound();
            }
            return View(taskTodo);
        }

        // POST: TaskTodoes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("TaskTodoID,Type")] TaskTodo taskTodo)
        {
            if (id != taskTodo.TaskTodoID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(taskTodo);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TaskTodoExists(taskTodo.TaskTodoID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(taskTodo);
        }

        // GET: TaskTodoes/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var taskTodo = await _context.TaskTodos
                .FirstOrDefaultAsync(m => m.TaskTodoID == id);
            if (taskTodo == null)
            {
                return NotFound();
            }

            return View(taskTodo);
        }

        // POST: TaskTodoes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var taskTodo = await _context.TaskTodos.FindAsync(id);
            _context.TaskTodos.Remove(taskTodo);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TaskTodoExists(string id)
        {
            return _context.TaskTodos.Any(e => e.TaskTodoID == id);
        }
    }
}
