﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Attendance.ViewModel
{
    public class EmployeeViewModel 
    {
        [Required, Display(Name = "First name")]
        public string FirstName { get; set; }
        [Required, Display(Name = "Surname")]
        public string LastName { get; set; }

        public string Title { get; set; }
        [Required]
        public string Contact { get; set; }
        public string Email { get; set; }

        [Required, DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true), Display(Name = "Date of Birth")]
        public DateTime DOB { get; set; }
        [Required]
        public DateTime RecruitmentDate { get; set; }
        public string ResidenceAddress { get; set; }
        public string OtherContact { get; set; }
        public EmployeeType EmpType { get; set; }
        [Required]
        public Sex Gender { get; set; }
        [Required]
        public Marital MaritalStatus { get; set; }

        [Required, Display(Name ="Job Title")]
        public Jobtitles Jobtitle { get; set; }

        public enum Jobtitles
        {
            Intern, Assistant, Associate, Manager
        }
        public enum Sex
        {
            Male, Female
        }

        public enum Marital { Married, Single, Divorced, Other }

        public enum EmployeeType
        {
            Permanent, Contract, Service, Intern
        }
    }
}
