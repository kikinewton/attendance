﻿using Attendance.Misc;
using AttendanceData.Entities;
using System;
using System.ComponentModel.DataAnnotations;

namespace Attendance.ViewModel
{
    public class EmployeeTaskViewModel
    {
        public string TaskTodoId { get; set; }
        [Required, Display(Name = "Task"), MaxLength(50)]
        public string TaskType { get; set; }
        [MaxLength(256), DataType(DataType.MultilineText)]
        public string Description { get; set; }

        ////public int EmployeeID { get; set; }
        //[Required, Display(Name = "Start Date")]

        //public Employee Employee { get; set; }
        //[Required, Display(Name = "Start Date & Time"), DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        
        public DateTime StartDate { get; set; }
        [Required, Display(Name = "End Date & Time"), DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [DateGreaterThan(nameof(StartDate))]
        public DateTime EndDate { get; set; }
        [Required, Display(Name = "Status")]
        public bool IsComplete { get; set; }

        //public double Duration { get; set; }
    }
}
