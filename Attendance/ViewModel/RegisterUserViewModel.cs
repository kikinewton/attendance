﻿using AttendanceData.Entities;
using System.ComponentModel.DataAnnotations;

namespace Attendance.ViewModel
{
    public class RegisterUserViewModel
    {
        [Required, MaxLength(50), Display(Name = "First Name")]
        public string FirstName { get; set; }
        [Required, MaxLength(50), Display(Name ="Last Name")]
        public string LastName { get; set; }
        [Required, DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        [Required]
        public Sex Gender { get; set; }
        [Required, Display(Name ="Marital Status")]
        public Marital MaritalStatus { get; set; }
        [Required, DataType(DataType.PhoneNumber), Display(Name ="Phone Number")]
        public string PhoneNumber { get; set; }
        public int Department { get; set; }
        [Required, MaxLength(50)]
        public string Username { get; set; }
        [Required, DataType(DataType.Password)]
        public string Password { get; set; }
        [DataType(DataType.Password), Compare(nameof(Password)), Display(Name ="Confirm Password")]
        public string ConfirmPassword { get; set; }

        [Required, Display(Name = "Job Title")]
        public Jobtitles Jobtitle { get; set; }
        public enum Jobtitles
        {
            Intern, Assistant, Associate, Manager
        }
        public enum Sex
        {
            Male, Female
        }

        public enum Marital { Married, Single, Divorced, Other }

        public enum EmployeeType
        {
            Permanent, Contract, Service, Intern
        }
    }
}
