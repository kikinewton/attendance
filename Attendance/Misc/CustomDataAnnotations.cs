﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Attendance.Misc
{
    public class CurrentDateAtribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            var date = (DateTime)value;
            return date >= DateTime.Now;
        }
    }

    //public class CompareDate : ValidationAttribute
    //{
    //    public DateTime MinDate { get; set; }

    //    public override bool IsValid(object value)
    //    {
    //        var dateValue = (DateTime)value;
    //        return dateValue >= MinDate;
    //    }
    //}

    public class DateGreaterThan : ValidationAttribute
    {
        private string _startDatePropertyName;
        public DateGreaterThan(string startDatePropertyName)
        {
            _startDatePropertyName = startDatePropertyName;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var propertyInfo = validationContext.ObjectType.GetProperty(_startDatePropertyName);

            if (propertyInfo == null)
            {
                return new ValidationResult(string.Format("Unknown property {0}", _startDatePropertyName));
            }

            var propertyValue = propertyInfo.GetValue(validationContext.ObjectInstance, null);

            if ((DateTime)value > (DateTime)propertyValue)
            {
                return ValidationResult.Success;
            }
            else
            {
                var startDateDisplayName = propertyInfo
                    .GetCustomAttributes(typeof(DisplayAttribute), true)
                    .Cast<DisplayAttribute>()
                    .Single()
                    .Name;

                return new ValidationResult(validationContext.DisplayName + " must be later than " + startDateDisplayName + ".");
            }
        }
    }


}
