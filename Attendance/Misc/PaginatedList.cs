﻿using AttendanceData.Entities;
using AttendanceServices.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System;
using System.Collections.Generic;

namespace Attendance.Misc
{
    public class PaginationModel : PageModel
    {
        private IEmployeeTaskData _emptaskdata;

        public PaginationModel(IEmployeeTaskData data)
        {
            _emptaskdata = data;
        }
        //public PaginatedList(List<T> items, int count, int pageIndex,int pageSize)
        //{
        //    PageIndex = pageIndex;
        //    TotalPage = (int)Math.Ceiling(count / (double)pageSize);
        //    this.AddRange(items);
        //}
        //public int PageIndex { get; private set; }
        //public int TotalPage { get; private set; }

        //public bool HasPreviousPage => (PageIndex > 1);

        //public bool HasNextPage => PageIndex < TotalPage;

        //public static async Task<PaginatedList<T>> CreateAsync(IQueryable<T> source, int pageIndex,int pageSize)
        //{
        //    var count = await source.CountAsync();
        //    var items = await source.Skip((pageIndex - 1) * pageSize).Take(pageSize).ToListAsync();
        //    return new PaginatedList<T>(items, count, pageIndex, pageSize);

        //}

        public int Count { get; set; }
        public int PageSize { get; set; }
        [BindProperty(SupportsGet = true)]
        public int CurrentPage { get; set; }
        public int TotalPages => (int)Math.Ceiling(Count / (double)PageSize);
        public List<EmployeeTask> Data { get; set; }

        //public async Task GetTaskAsync()
        //{
        //    //Data = await _emptaskdata.ge
        //}
    }
}
