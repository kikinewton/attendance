﻿using AttendanceData.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Attendance.Models
{
    public class EmployeeTaskModel
    {
        private const string V = "End Date & Time";

        [Required, Display(Name = "Task"), MaxLength(50)]
        public string TaskType { get; set; }
        [MaxLength(256)]
        public string Description { get; set; }
        [Display(Name = "Employee")]
        public string UserID { get; set; }

        public int TaskTodoId { get; set; }
        public Employee Employee { get; set; }
        [Required, Display(Name = "Start Date & Time"), DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true), DataType(DataType.DateTime)]
        public DateTime StartDate { get; set; }
        [Required, Display(Name = V), DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true), DataType(DataType.DateTime)]
        public DateTime EndDate { get; set; }
        [Display(Name = "Completed")]
        public bool IsComplete { get; set; }
        public IEnumerable<EmployeeTask> employeeTasks { get; set; }
    }
}
