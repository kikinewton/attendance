﻿using AttendanceData.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Attendance.Models
{
    public class EmployeeModel
    {
        public IQueryable<Employee> Employees { get; set; }
    }
}
