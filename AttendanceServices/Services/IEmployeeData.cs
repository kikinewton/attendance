﻿
using AttendanceData.Entities;
using System.Collections.Generic;

namespace AttendanceServices.Services
{
    public interface IEmployeeData
    {
        IEnumerable<Employee> GetAll();
        Employee GetById(string id);
        void Commit();
        Employee Add(Employee employee);
        string GetEmployeeName(string id);
        //string GetJobtitle(string )
        
    }
}
