﻿using System;
using System.Collections.Generic;
using System.Linq;
using AttendanceData.Entities;

namespace AttendanceServices.Services
{
    public class TaskDataService : ITaskTodoData
    {
        private AttendanceDbContext _context;

        public TaskDataService(AttendanceDbContext context)
        {
            _context = context;
        }
        public TaskTodo Add(TaskTodo task)
        {
            _context.Add(task);
            _context.SaveChanges();
            return task;
        }



        public IEnumerable<TaskTodo> GetAll() => _context.TaskTodos;

        public string GetTaskId(string _task_name)
        {
            TaskTodo item = _context.TaskTodos.FirstOrDefault(x => x.Type == _task_name);
            return item.TaskTodoID;
        }
    }
}
