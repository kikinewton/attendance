﻿using AttendanceData.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace AttendanceServices.Services
{
    public interface IDepartmentData
    {
        IEnumerable<Department> GetAll();
        Department Add(Department department);
        int GetId(string department_name);
        string GetName(int id);
        //List<string> GetAllName();

    }
}
