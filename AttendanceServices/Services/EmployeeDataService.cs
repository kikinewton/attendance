﻿using System.Collections.Generic;
using System.Linq;

using AttendanceData.Entities;

namespace AttendanceServices.Services
{
    public class EmployeeDataService : IEmployeeData
    {
        private AttendanceDbContext _context;

        public EmployeeDataService(AttendanceDbContext context) => _context = context;
        public Employee Add(Employee employee)
        {
            _ = _context.Add(employee);
            _context.SaveChanges();
            return employee;

        }

        public void Commit() => _context.SaveChanges();

        public IEnumerable<Employee> GetAll() => _context.Employees;

        public Employee GetById(string id) => _context.Employees.FirstOrDefault(x => x.EmployeeId == id);

        public string GetEmployeeName(string id)
        {
            string firstname = GetById(id).FirstName;
            string lastname = GetById(id).LastName;
            return firstname + " " + lastname;
        }
    }
}
