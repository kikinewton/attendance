﻿using AttendanceData.Entities;
using System.Collections.Generic;
using System.Linq;


namespace AttendanceServices.Services
{
    public class EmployeeTaskDataService : IEmployeeTaskData
    {
        private AttendanceDbContext _context;

        public EmployeeTaskDataService(AttendanceDbContext context) => _context = context;
        public EmployeeTask Add(EmployeeTask employeeTask)
        {
            _ = _context.Add(employeeTask);
            _context.SaveChanges();
            return employeeTask;
        }

        public void Commit() => _context.SaveChanges();

        public void Delete(int id)
        {
            var item = _context.EmployeeTasks.FirstOrDefault(x => x.ID == id);
            _context.EmployeeTasks.Remove(item);
            _context.SaveChanges();
        }

        public IEnumerable<EmployeeTask> GetAll() => _context.EmployeeTasks;

        public EmployeeTask GetById(int id)
        {
            var item = _context.EmployeeTasks.FirstOrDefault(x => x.ID == id);
            return item;
        }

        public int GetDurationByProject(string id)
        {
            var todoItem = _context.EmployeeTasks.Where(x => x.TodoId == id);
            double d;
            foreach(var i in todoItem)
            {
                d = i.Duration;
                return (int)d;
            }
            return 0;
            
        }

        public int GetTaskByUserId(string UserId)
        {
            var item = _context.EmployeeTasks.Where(x => x.UserId == UserId && x.IsComplete == true).Count();
            return item;
        }
        
        
    }
}
