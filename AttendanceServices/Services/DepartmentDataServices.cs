﻿using AttendanceData.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AttendanceServices.Services
{
    public class DepartmentDataServices : IDepartmentData
    {
        private AttendanceDbContext _context;

        public DepartmentDataServices(AttendanceDbContext context) => _context = context;

        public Department Add(Department department)
        {
            _context.Add(department);
            _context.SaveChanges();
            return department;
        }

        public IEnumerable<Department> GetAll()
        {
            return _context.Departments;
        }

        //public List GetAllName()
        //{
        //   return _context.Departments.Select(x => x.GetType().GetProperty("Name").GetValue(x, null)).ToArray();
        //}

        public int GetId(string department_name) => _context.Departments.FirstOrDefault(x => x.Name == department_name).DepartmentId;

        public string GetName(int id) => _context.Departments.FirstOrDefault(x => x.DepartmentId == id).Name;

        
        
    }
}
