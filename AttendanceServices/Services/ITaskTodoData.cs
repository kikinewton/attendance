﻿using AttendanceData.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace AttendanceServices.Services
{
    public interface ITaskTodoData
    {
        string GetTaskId(string _task_name);
        IEnumerable<TaskTodo> GetAll();
        TaskTodo Add(TaskTodo taskTodo);

    }
}
