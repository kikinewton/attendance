﻿
using AttendanceData.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace AttendanceServices.Services
{
    public interface IEmployeeTaskData
    {
        EmployeeTask GetById(int id);
        IEnumerable<EmployeeTask> GetAll();

        EmployeeTask Add(EmployeeTask employeeTask);
        
        void Delete(int id);
        int GetTaskByUserId(string UserId);
        void Commit();
        int GetDurationByProject(string id);
    }
}
